var App = {
    Common : {}
}
var editImgLock = 0;
App.Common = (function ($,  undefined) {
    var instance = {};

    instance.init = function(){   
        $("span.text:not(.isactive)").on("click", switchToInput);

       $('.imageEditorControls .btn-change-image, .editorChooseImage a').click(function () {       
            $('#cvoFormImageFile').click();
        });

       $('#cvoFormImageFile').on('change',function(){
           //upload file
            alert(12)
       });

       $('#curriculum-vitae').on('click','.fieldgroup_controls .clone',function(){
            var $emlement = $(this).closest('.cvo-block').find('.fieldgroup_container').find('.root');         
             if($emlement.length > 0) {   
                instance.addFielGroup($($emlement).clone().removeClass('root'), $(this), instance.reInitSpanToText);   
            }                
       });

       $('#curriculum-vitae').on('click','.fieldgroup_controls .remove',function(){
            instance.removeFileGroup($(this));            
       });

   }

   instance.emlementFielControl = function(){
        var $emlement = '<div class="fieldgroup_controls">'
            $emlement += '<div class="clone"><i class="fa fa-plus"></i> Thêm</div>'
            $emlement += '<div class="remove"><i class="fa fa-minus"></i> Xóa</div>'
            $emlement += '</div>'
            return $emlement;
   }

   instance.addFielGroup = function($element, $control, callbackFunc){

        $control.closest('.cvo-block').find('.fieldgroup_container').find('.fieldgroup').last().after($element);
        if (typeof (callbackFunc) == 'function') {
           callbackFunc();
       }
   }

   instance.removeFileGroup = function($control, callbackFunc){
        var $elements = $control.closest('.cvo-block').find('.fieldgroup_container').find('.fieldgroup');
        if($elements.length > 1) {
            $elements.last().remove();
        }
        
        if (typeof (callbackFunc) == 'function') {
           callbackFunc();
       }
   }

   instance.reInitSpanToText = function(){
        $("span.text").unbind( "click" );
        $("span.text:not(.isactive)").on("click", switchToInput);
   }

    var switchToInput = function () {       
        var $input = $("<input>", {
            val: $(this).text(),
            type: "text"
        });
        //$input.attr("ID", "loadNum");
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
       
    };

    var switchToSpan = function () {
        var $span = $("<span>", {
            text: $(this).val()
        });

        $span.addClass('text');

        $(this).replaceWith($span);
        $span.on("click", switchToInput);
    }

    return instance;
}(jQuery));
