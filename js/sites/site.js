
/* Handle auto saving */
function startAutoSavingInterval()
{
    if (autoSaver) {
        clearInterval(autoSaver);
    }
    autoSaver = setInterval(function () {
        autoSave();
    }, 60000);
}
/* ========= Handle auto saving */
/* Handle recovering data */
function loadCvLayoutWithRecoveryData()
{
    loadCvLayout('', 'default', 'vi', false, false, true);
    $('#modal-recover-unsaved-cv').modal('hide');
    startAutoSavingInterval();
}
function cancelRecoveryData()
{
    $('#modal-recover-unsaved-cv').modal('hide');
    startAutoSavingInterval();
}
/* === Handle recovering data === */
$(document).ready(function () {
        $('#modal-recover-unsaved-cv').modal('show');
    });
var editImgLock = 0;
var printOptions = {"margins":{"top":15,"right":15,"bottom":5,"left":15}};
$(document).ready(function () {
    CVOFormUtils.bindImageEditorHandler(cropperImageEditor);
    initImageControls();
    // ========
    $('.imageEditorControls .btn-save-image').click(function () {
        if (editImgLock !== 0) {
            return;
        }
        saveAvatar();
    });
    $('.imageEditorControls .btn-change-image, .editorChooseImage a').click(function () {
        if (editImgLock !== 0) {
            return;
        }
        uploadAvatar();
    });
    $('.imageEditorControls .btn-remove-image').click(function () {
        if (editImgLock !== 0) {
            return;
        }
        setNoImage();
    });
    $('.imageEditorControls .btn-close-image-editor').click(function () {
        closeImageEditor();
    });
    // Handle drop area
    var dropArea = $(".editorChooseImage .btn-choose-image");
    dropArea.on('dragenter', function (e)
    {
        e.stopPropagation();
        e.preventDefault();
    });
    dropArea.on('dragover', function (e)
    {
         e.stopPropagation();
         e.preventDefault();
    });
    dropArea.on('drop', function (e)
    {
        e.preventDefault();
        var files = e.originalEvent.dataTransfer.files;
        //We need to send dropped files to Server
        dropUploadAvatar(files, "", "https://www.topcv.vn/upload-avatar");
    });
    // Init paging-separator
    PagingSeparator.init({printOptions: printOptions});
    $('#cvo-document-root').on('mresize', function () {
        PagingSeparator.render();
    });
});
