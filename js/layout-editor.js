var LayoutEditorControler = {
    cvoLayout: null,
    init: function(_0x812bx3) {
        if (undefined === _0x812bx3) {
            _0x812bx3 = false
        };
        var _0x812bx4 = this;
        this[_0xa268[0]] = {};
        $[_0xa268[3]](CVOFormController[_0xa268[2]][_0xa268[1]], function(_0x812bx5, _0x812bx6) {
            _0x812bx4[_0xa268[0]][_0x812bx5] = []
        });
        this[_0xa268[4]](CVOFormController[_0xa268[0]], true);
        if (_0x812bx3) {
            $(_0xa268[14])[_0xa268[13]](function(_0x812bx7) {
                var _0x812bx8 = $(this)[_0xa268[5]]();
                var _0x812bx9 = _0x812bx8[_0xa268[7]](_0xa268[6]);
                var _0x812bx5 = _0x812bx8[_0xa268[7]](_0xa268[8]);
                if (_0x812bx8[_0xa268[10]](_0xa268[9])) {
                    _0x812bx4[_0xa268[11]](_0x812bx5, _0x812bx9)
                } else {
                    _0x812bx4[_0xa268[12]](_0x812bx5, _0x812bx9)
                }
            });
            $(_0xa268[16])[_0xa268[13]](function() {
                _0x812bx4[_0xa268[15]]();
                hideLayoutEditor()
            });
            $(_0xa268[19])[_0xa268[13]](function(_0x812bx7) {
                _0x812bx7[_0xa268[17]]();
                _0x812bx4[_0xa268[18]]()
            });
            $(_0xa268[21])[_0xa268[13]](function(_0x812bx7) {
                _0x812bx7[_0xa268[17]]();
                _0x812bx4[_0xa268[20]]()
            });
            $(_0xa268[23])[_0xa268[13]](function(_0x812bx7) {
                _0x812bx7[_0xa268[17]]();
                _0x812bx4[_0xa268[22]]()
            });
            $(_0xa268[24])[_0xa268[13]](function() {
                hideLayoutEditor()
            })
        };
        return _0x812bx4
    },
    renderLayoutEditor: function(_0x812bxa, _0x812bxb) {
        var _0x812bx4 = this;
        this[_0xa268[22]]();
        $[_0xa268[3]](_0x812bxa, function(_0x812bx5, _0x812bxc) {
            _0x812bx4[_0xa268[0]][_0x812bx5] = [];
            var _0x812bxd = _0x812bxc[_0xa268[25]] - 1;
            for (var _0x812bxe = _0x812bxd; _0x812bxe >= 0; _0x812bxe--) {
                _0x812bx4[_0xa268[12]](_0x812bx5, _0x812bxc[_0x812bxe], true)
            };
            if (_0x812bxb) {
                _0x812bx4[_0xa268[26]](_0x812bx5)
            }
        })
    },
    resetLayout: function() {
        this[_0xa268[4]](CVOFormController[_0xa268[27]], false)
    },
    enableBlock: function(_0x812bx5, _0x812bx9, _0x812bxf) {
        if (undefined === _0x812bxf) {
            _0x812bxf = false
        };
        if (this[_0xa268[0]][_0x812bx5][_0xa268[28]](_0x812bx9) < 0) {
            this[_0xa268[0]][_0x812bx5][_0xa268[29]](_0x812bx9)
        };
        var _0x812bx8 = this[_0xa268[30]](_0x812bx9);
        if (_0x812bxf) {
            _0x812bx8[_0xa268[32]](this[_0xa268[31]](_0x812bx5))
        };
        _0x812bx8[_0xa268[33]](_0xa268[9])
    },
    disableBlock: function(_0x812bx5, _0x812bx9) {
        var _0x812bx10 = CVOFormController[_0xa268[34]](_0x812bx5);
        if (_0x812bx10[_0xa268[35]]) {
            return false
        };
        this[_0xa268[30]](_0x812bx9)[_0xa268[36]](_0xa268[9]);
        if (this[_0xa268[0]][_0x812bx5][_0xa268[28]](_0x812bx9) < 0) {
            return false
        };
        var _0x812bxc = [];
        for (var _0x812bxe = 0; _0x812bxe < this[_0xa268[0]][_0x812bx5][_0xa268[25]]; _0x812bxe++) {
            var _0x812bx11 = this[_0xa268[0]][_0x812bx5][_0x812bxe];
            if (_0x812bx11 !== _0x812bx9) {
                _0x812bxc[_0xa268[29]](_0x812bx11)
            }
        };
        this[_0xa268[0]][_0x812bx5] = _0x812bxc
    },
    disableAllBlocks: function() {
        var _0x812bx4 = this;
        $[_0xa268[3]](CVOFormController[_0xa268[2]][_0xa268[1]], function(_0x812bx5, _0x812bx6) {
            var _0x812bxd = _0x812bx6[_0xa268[37]][_0xa268[25]] - 1;
            for (var _0x812bxe = _0x812bxd; _0x812bxe >= 0; _0x812bxe--) {
                _0x812bx4[_0xa268[11]](_0x812bx5, _0x812bx6[_0xa268[37]][_0x812bxe])
            }
        })
    },
    enableAllBlocks: function() {
        var _0x812bx4 = this;
        $[_0xa268[3]](CVOFormController[_0xa268[2]][_0xa268[1]], function(_0x812bx5, _0x812bx6) {
            var _0x812bxd = _0x812bx6[_0xa268[37]][_0xa268[25]] - 1;
            for (var _0x812bxe = _0x812bxd; _0x812bxe >= 0; _0x812bxe--) {
                _0x812bx4[_0xa268[12]](_0x812bx5, _0x812bx6[_0xa268[37]][_0x812bxe])
            }
        })
    },
    finish: function() {
        for (var _0x812bx5 in this[_0xa268[0]]) {
            this[_0xa268[0]][_0x812bx5] = this[_0xa268[40]](_0x812bx5)[_0xa268[39]](function() {
                return $(this)[_0xa268[7]](_0xa268[6])
            })[_0xa268[38]]()
        };
        CVOFormController[_0xa268[41]](this[_0xa268[0]])
    },
    refresh: function(_0x812bx5) {
        var _0x812bx10 = CVOFormController[_0xa268[34]](_0x812bx5);
        if (!_0x812bx10[_0xa268[35]]) {
            $(_0xa268[44] + _0x812bx5 + _0xa268[45])[_0xa268[43]]({
                items: _0xa268[42]
            })
        }
    },
    getBlock: function(_0x812bx9) {
        return $(_0xa268[46] + _0x812bx9 + _0xa268[45])
    },
    getActiveBlocks: function(_0x812bx5) {
        return $(_0xa268[44] + _0x812bx5 + _0xa268[47])
    },
    getGroup: function(_0x812bx5) {
        return $(_0xa268[44] + _0x812bx5 + _0xa268[45])
    }
}